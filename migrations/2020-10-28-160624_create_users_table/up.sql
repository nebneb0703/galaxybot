-- Your SQL goes here
CREATE TABLE users (
  discord_id text NOT NULL,
  osu_id text NOT NULL,
  osu_username text NOT NULL,
  default_mode smallint NOT NULL,
  auto_guild_id text NOT NULL,
  PRIMARY KEY (discord_id),
  UNIQUE (osu_id, osu_username)
);