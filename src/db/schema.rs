table! {
    guilds (guild_id) {
        guild_id -> Text,
        auto_channel_id -> Text,
    }
}

table! {
    users (discord_id) {
        discord_id -> Text,
        osu_id -> Text,
        osu_username -> Text,
        default_mode -> Int2,
        auto_guild_id -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    guilds,
    users,
);
