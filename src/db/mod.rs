pub mod schema;

mod user;
pub use user::*;

mod guilds;
pub use guilds::*;

use diesel::prelude::*;

use tokio::sync::Mutex;

pub struct Db {
    pub conn: Mutex<PgConnection>,
}

impl Db {
    pub fn new() -> Self {
        let config = std::env::var("DATABASE_URL").expect("Config required in environmental variable DATABASE_URL.");
        let conn = PgConnection::establish(&config).expect("Could not connect to database.");

        Db { conn: Mutex::new(conn) }
    }
}
