use crate::db::schema::users;
use crate::db::*;

use nebbot_utils::types::{ TextU64, TextU32 };

use diesel::prelude::*;

use crate::osu::*;

#[derive(Clone, Debug, Queryable, Insertable, AsChangeset, Identifiable)]
#[table_name = "users"]
#[primary_key(discord_id)]
pub struct DbUser {
    pub discord_id: TextU64,
    pub osu_id: TextU32,
    pub osu_username: String,
    pub default_mode: OsuMode,
    pub auto_guild_id: TextU64,
}

impl DbUser {
    pub fn default(discord_id: u64, osu_id: u32, osu_username: String, default_mode: OsuMode) -> Self {
        DbUser {
            discord_id: TextU64(discord_id), osu_id: TextU32(osu_id), osu_username, default_mode,
            auto_guild_id: TextU64(0),
        }
    }
}

impl Db {
    pub async fn get_user_by_osu(&self, osu: &Osu, account: OsuAccount) -> QueryResult<Option<DbUser>> {
        let osu_id = match account.find_id(osu).await {
            Some(o) => o,
            None => { return Ok(None); },
        };

        users::table.filter(users::osu_id.eq(TextU32(osu_id)))
            .first(&*self.conn.lock().await).optional()
    }

    pub async fn get_user(&self, discord_id: u64) -> QueryResult<Option<DbUser>> {
        users::table.find(discord_id.to_string())
            .first(&*self.conn.lock().await).optional()
    }

    pub async fn add_user(&self, user: DbUser) -> QueryResult<()> {
        diesel::insert_into(users::table)
            .values(user).execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn remove_user(&self, discord_id: u64) -> QueryResult<()> {
        diesel::delete(users::table)
            .filter(users::discord_id.eq(TextU64(discord_id))).execute(&*self.conn.lock().await)?;

        Ok(())
    }
}