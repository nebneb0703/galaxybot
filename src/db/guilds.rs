use crate::db::schema::guilds;
use crate::db::*;

use nebbot_utils::types::TextU64;

use diesel::prelude::*;

#[derive(Clone, Debug, Queryable, Insertable, AsChangeset, Identifiable)]
#[table_name = "guilds"]
#[primary_key(guild_id)]
pub struct DbGuild {
    pub guild_id: TextU64,
    pub auto_channel_id: TextU64,
}

impl DbGuild {
    pub fn new(guild_id: u64) -> Self {
        DbGuild {
            guild_id: TextU64(guild_id), auto_channel_id: TextU64(0),
        }
    }
}

impl Db {
    pub async fn get_guild(&self, guild_id: u64) -> QueryResult<Option<DbGuild>> {
        guilds::table.find(guild_id.to_string())
            .first(&*self.conn.lock().await).optional()
    }

    pub async fn add_guild(&self, guild: DbGuild) -> QueryResult<()> {
        diesel::insert_into(guilds::table)
            .values(guild).execute(&*self.conn.lock().await)?;

        Ok(())
    }
}