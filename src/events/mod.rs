use serenity:: {
    async_trait,

    client::bridge::gateway::event::ShardStageUpdateEvent,
    gateway::ConnectionStage,

    model::{ 
        channel::{ Channel, GuildChannel },
        gateway::{ Ready, Activity },
        event::{ PresenceUpdateEvent, ResumedEvent },
    },
};

use serenity::prelude::*;
use chrono::prelude::*;

use serenity::utils::Colour;

use std::sync::Arc;

use crate::osu::*;
use crate::db::*;

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);
    }

    async fn resume(&self, _: Context, resume: ResumedEvent) {
        info!("Resumed! Trace: {:?}",  resume.trace);
    }

    async fn shard_stage_update(&self, _ctx: Context, update: ShardStageUpdateEvent) {
        
        info!("Shard {} update event: Old: {:?}; New: {:?}", update.shard_id, update.old, update.new);

        if let ConnectionStage::Connecting = update.new {
            warn!("Shard {} has disconnected! Reconnecting...", update.shard_id);
        }

        if let ConnectionStage::Resuming = update.new {
            warn!("Resuming Shard {}...", update.shard_id);
        }
    }

    async fn presence_update(&self, ctx: Context, update: PresenceUpdateEvent) {
        let time = Utc::now();

        debug!("Presence update");

        let mut osu_activity: Option<Activity> = None; 
        for a in update.presence.activities {
            debug!("{:?}", a);
            if let Some(id) = a.application_id {
                if *id.as_u64() == crate::osu::APPLICATION_ID {
                    osu_activity = Some(a);
                    break;
                }
            }
        }

        if let Some(a) = osu_activity {
            debug!("Found osu activity for user {}", update.presence.user_id);

            let assets = match a.clone().assets {
                Some(a) => a,
                None => {
                    /*let _users = ctx.cache.users().await;

                    let dc_user = _users.get(&update.presence.user_id).unwrap();

                    dc_user.direct_message(&ctx.http, |m| m.embed(|e| e
                        .title("Error with osu! rich presence. Restart the game?")
                        .colour(Colour::from_rgb(228, 132, 250))
                        .footer(|f| f
                            .icon_url(dc_user.avatar_url().unwrap_or(dc_user.default_avatar_url()))
                            .text(dc_user.tag())
                        )
                    
                    )).await.unwrap();*/

                    return;
                },
            };

            let large_text = assets.large_text.unwrap();
            let username = if let Some(un) = large_text.split_whitespace().next() { un } else { " " };

            let account = OsuAccount::from(username);
            let mode = match OsuMode::parse(assets.small_text.unwrap().as_str()) {
                Some(m) => m,
                None => OsuMode::Standard,
            };

            let data = ctx.data.read().await;
            let osu = data.get::<crate::OsuContainer>().expect("Couldn't retrieve OsuContainer");
            let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");
        
            if let Ok(o) = database.get_user(*update.presence.user_id.as_u64()).await {
                if let None = o {
                    let _users = ctx.cache.users().await;
                    let dc_user = _users.get(&update.presence.user_id).unwrap();
    
                    let profile = osu.get_user(account.clone(), mode).await.unwrap();
    
                    let user = DbUser::default(*dc_user.id.as_u64(), profile.user_id,
                        profile.username.clone(), mode.clone());
    
                    match database.add_user(user.clone()).await {
                        Ok(_) => {
                            dc_user.direct_message(&ctx.http, |m| m.embed(|e| e
                                .title(format!("Synced {} with user {}", dc_user.tag(), user.osu_username))
    
                                .thumbnail(format!("{}/{}", PROFILE_PIC_URL, user.osu_id.0))
                                .colour(Colour::from_rgb(228, 132, 250))
                                .url(format!("{}/{}/{}", PROFILE_URL, user.osu_id.0, mode.url()))
                                .footer(|f| f
                                    .icon_url(dc_user.avatar_url().unwrap_or(dc_user.default_avatar_url()))
                                    .text(dc_user.tag())
                                )
                            
                            )).await.unwrap();
                        }
                        Err(e) => {
                            error!("{}", e);
                        }
                    }
                }
            }

            let state = a.clone().state.unwrap();

            let song = a.clone().details;
            let presence = OsuPresence { account: account.clone(), state: state.clone(), mode, song, time };

            let _ch_prsnc = osu.get_cached_presence(presence.account.clone()).await;
            
            osu.cache_presence(presence.clone()).await;

            let cached_presence = match _ch_prsnc {
                Some(c) => c,
                None => return,
            };

            if cached_presence.state == "Idle" || state == "AFK" { return; }

            debug!("Cached Presence: {:?}", cached_presence);

            if state == "Idle" {

                let discord_id = *update.presence.user_id.as_u64();

                let user = if let Ok(op_u) = database.get_user(discord_id).await {
                    if let Some(u) = op_u {
                        u
                    } else { return; }
                } else { return; };

                let guild = if let Ok(op_g) = database.get_guild(user.auto_guild_id.0).await{
                    if let Some(g) = op_g {
                        g
                    } else { return; }
                } else { return; };

                let channel = if let Channel::Guild(c) = ctx.cache.channel(guild.auto_channel_id.0).await.unwrap() { c } else {
                        error!("Could not get GuildChannel {} from server {}", guild.auto_channel_id.0, user.auto_guild_id.0);

                        return;
                    };

                check_latest(osu.clone(), cached_presence, channel, ctx.http.clone());
            }
        }
    }
}

use serenity::http::client::Http;
use tokio::time::sleep;
use tokio::time::Duration;

const INITIAL_DELAY: u64 = 1;
const WAIT_TIME: u64 = 10;

fn check_latest(osu: Arc<Osu>, cached_presence: OsuPresence,
    channel: GuildChannel, http: Arc<Http>) {

    tokio::spawn(async move {

        sleep(Duration::from_secs(INITIAL_DELAY)).await;

        let mut tries: u8 = 0;

        while tries <= 3 {
            let recents = osu.get_recent(cached_presence.account.clone(), cached_presence.mode.clone(), 1).await
                .expect("Error getting recent score");
            
            let recent = match recents.first() {
                Some(r) => r,
                None => {

                    debug!("No recent found {:?}", cached_presence);

                    tries += 1;

                    sleep(Duration::from_secs(WAIT_TIME)).await;

                    continue;
                }
            };

            if !recent.compare(cached_presence.clone()) {
                
                debug!("Recent doesn't match {:?}\n{:?}", recent, cached_presence);

                tries += 1;

                sleep(Duration::from_secs(WAIT_TIME)).await;

                continue;
            }

            if let OsuGrade::Fail = recent.rank { 
                debug!("Recent is a fail: {:?}", recent);

                return; 
            }
            else {

                // TODO: Configurables
            
                let profile = match osu.get_user(cached_presence.account.clone(), cached_presence.mode.clone()).await {
                    Ok(p) => p,
                    Err(_) => return,
                };

                recent.embed(http.clone(), channel.id, profile).await.expect("Error sending auto message");

            }

            return;
        }

        debug!("Failed after 3 tries: {:?}", cached_presence);
    });
}
