#[macro_use]
extern crate bitflags;

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate log;

use serenity:: {
    client::bridge::gateway::GatewayIntents,
    framework::standard::{
        StandardFramework,
    },
    http::Http,
};

use serenity::prelude::*;

use std::env;
use std::sync::Arc;
use std::collections::HashSet;

mod cache;
use cache::*;

mod db;
use db::Db;

mod osu;
use osu::Osu;

mod commands;
use commands::*;

mod events;
use events::*;

#[tokio::main]
async fn main() {
    dotenv::dotenv().unwrap();

    let format = |out: fern::FormatCallback, message: &std::fmt::Arguments, record: &log::Record| {
        let level_colour = match record.level() {
            log::Level::Error => "\x1b[38;5;9m",
            log::Level::Warn => "\x1b[38;5;11m",
            log::Level::Debug => "\x1b[38;5;14m",
            
            log::Level::Info | 
            log::Level::Trace => "",
        };

        out.finish(format_args!(
            "\x1b[38;5;238m{}\x1b[0m [{}] {}{}\x1b[0m: {}",
            chrono::Utc::now().format("<%F %T>"),
            record.module_path().unwrap_or(record.target()),
            level_colour, record.level(),
            message
        ))
    };

    let now = chrono::Utc::now().timestamp();

    fern::Dispatch::new()
    .format(format)
    .level(log::LevelFilter::Warn)
    .level_for("galaxy_bot", log::LevelFilter::Debug)
    .chain(fern::log_file(format!("logs/debug_{}.txt", now)).expect("Failed to open log file."))
    .chain(
        fern::Dispatch::new()
        .format(format)
        .level_for("galaxy_bot", log::LevelFilter::Info)
        .chain(std::io::stdout())
        .chain(fern::log_file(format!("logs/log_{}.txt", now)).expect("Failed to open log file."))
    )
    .apply()
    .expect("Failed to create fern dispatch.");

    let osu = Osu::new(env::var("OSU_TOKEN").expect("Token required in environmental variable OSU_TOKEN"));

    let db = Db::new();

    debug!("test");

    let token = env::var("DISCORD_TOKEN").expect("Token required in environmental variable DISCORD_TOKEN");

    let http = Http::new_with_token(&token);

    let (owners, bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        },
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    let mut client = Client::builder(&token)
        .event_handler(Handler)
        .intents(GatewayIntents::all())
        .framework(StandardFramework::new()
        .configure(|c| c
            .prefix("o!")
            .case_insensitivity(true)
            .owners(owners)
            .on_mention(Some(bot_id)))
        .help(&HELP)
        .group(&GENERAL_GROUP)
        .group(&OSUCOMMANDS_GROUP)
        .group(&ADMINCONFIG_GROUP)
        .after(commands::after)
        ).await
        .expect("Error creating client");

    info!("Created client!");
        
    info!("Listening for events!");

    {
        let mut data = client.data.write().await;

        data.insert::<OsuContainer>(Arc::new(osu));
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
        data.insert::<DbContainer>(Arc::new(db));
    }

    if let Err(why) = client.start().await {
        error!("{:?}", why);
    }
}