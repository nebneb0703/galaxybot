#[derive(Clone, Debug)]
pub enum OsuGrade {
    Fail,
    D,
    C,
    B,
    A,
    S,
    SS,
    SilverS,
    SilverSS
}

impl OsuGrade {
    pub fn from(grade: &str) -> Option<OsuGrade> {
        match grade.to_lowercase().as_str() {
            "f" => Some(OsuGrade::Fail),
            "d" => Some(OsuGrade::D),
            "c" => Some(OsuGrade::C),
            "b" => Some(OsuGrade::B),
            "a" => Some(OsuGrade::A),
            "s" => Some(OsuGrade::S),
            "x" => Some(OsuGrade::SS),
            "sh" => Some(OsuGrade::SilverS),
            "xh" => Some(OsuGrade::SilverSS),
            _ => None
        }
    }

    pub fn name(&self) -> &str {
        match self {
            OsuGrade::Fail => "Fail",
            OsuGrade::D => "D",
            OsuGrade::C => "C",
            OsuGrade::B => "B",
            OsuGrade::A => "A",
            OsuGrade::S => "S",
            OsuGrade::SS => "SS" ,
            OsuGrade::SilverS => "S+",
            OsuGrade::SilverSS => "SS+",
        }
    }

    /*pub fn internal(&self) -> &str {
        match self {
            OsuGrade::Fail => "f",
            OsuGrade::D => "d",
            OsuGrade::C => "c",
            OsuGrade::B => "b",
            OsuGrade::A => "a",
            OsuGrade::S => "s",
            OsuGrade::SS => "x",
            OsuGrade::SilverS => "sh",
            OsuGrade::SilverSS => "xh",
        }
    }*/
}