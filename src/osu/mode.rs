use diesel::prelude::*;
use diesel::sql_types::{ SmallInt };
use diesel::serialize::{ ToSql, Output };
use diesel::deserialize::{ FromSql };
use diesel::backend::Backend;

use serenity::model::guild::Emoji;
use serenity::http::Http;

use std::io::prelude::*;
use std::sync::Arc;

#[derive(Copy, Clone, Debug, AsExpression, PartialEq, Eq, Hash)]
#[sql_type = "SmallInt"]
pub enum OsuMode{
    Standard,
    Mania,
    Taiko,
    CatchTheBeat
}

const EMOJI_SERVER_ID: u64 = 748140668240527410;

impl OsuMode {
    pub fn parse(s: &str) -> Option<OsuMode> {
        match s.replace("!", "").to_ascii_lowercase().as_str() {
            "s" | "osu" | "os" | "stnd" | "ostd" | "ost" | "osustandard" |
            "std" | "ostandard" | "o" | "circle" | "circles" |
            "standard" => Some(OsuMode::Standard),
            
            "m" | "osumania" | "osum" | "omania" | "k" |
            "om" | "man" | "osuman" | "keys" | "key" | "piano" |
            "mania" => Some(OsuMode::Mania),

            "t" | "tai" | "tko" | "otk" |
            "tk" | "otaiko" | "osutaiko" | "drum" | "drums" |
            "taiko" => Some(OsuMode::Taiko),

            "c" | "ctbt" | "fruit" | "ocatch" | "octb" |
            "catch" | "catb" | "osuctb" | "osucatch" | "osucatchthebeat" |
            "ctb" => Some(OsuMode::CatchTheBeat),

            _ => None
        }
    }

    pub fn int(&self) -> u8 {
        match self {
            OsuMode::Standard => 0,
            OsuMode::Taiko => 1,
            OsuMode::CatchTheBeat => 2,
            OsuMode::Mania => 3,
        }
    }

    pub fn from_int(i: u8) -> OsuMode {
        match i {
            0 => OsuMode::Standard,
            1 => OsuMode::Taiko,
            2 => OsuMode::CatchTheBeat,
            3 => OsuMode::Mania,
            
            _ => OsuMode::Standard
        }
    }

    pub fn name(&self) -> &str {
        match self {
            OsuMode::Standard => "osu!",
            OsuMode::Taiko => "osu!taiko",
            OsuMode::CatchTheBeat => "osu!catch",
            OsuMode::Mania => "osu!mania",
        }
    }

    pub fn url(&self) -> &str {
        match self {
            OsuMode::Standard => "osu",
            OsuMode::Taiko => "taiko",
            OsuMode::CatchTheBeat => "fruits",
            OsuMode::Mania => "mania",
        }
    }

    pub async fn emoji(&self, http: Arc<Http>) -> Emoji {
        let id = match self {
            OsuMode::Standard => 766313785802752000,
            OsuMode::Taiko => 766313785681117215,
            OsuMode::CatchTheBeat => 766313785233113089,
            OsuMode::Mania => 766313785627508776,
        };

        http.get_emoji(EMOJI_SERVER_ID, id).await.expect("Could not retrieve emoji.")
    }
}

impl<DB, ST> Queryable<ST, DB> for OsuMode
where
    DB: Backend,
    i16: Queryable<ST, DB>,
{
    type Row = <i16 as Queryable<ST, DB>>::Row;

    fn build(row: Self::Row) -> Self {
        OsuMode::from_int(i16::build(row) as u8)
    }
}

impl<Db> ToSql<SmallInt, Db> for OsuMode
where 
    Db: Backend,
    i16: ToSql<SmallInt, Db>
{
    fn to_sql<W: Write>(&self, out: &mut Output<W, Db>) -> diesel::serialize::Result {
        (self.int() as i16).to_sql(out)
    }
}

impl<Db> FromSql<SmallInt, Db> for OsuMode
where 
    Db: Backend,
    i16: FromSql<SmallInt, Db>
{
    fn from_sql(bytes: Option<&Db::RawValue>) -> diesel::deserialize::Result<Self> {
        Ok(OsuMode::from_int(i16::from_sql(bytes)? as u8))
    }
}