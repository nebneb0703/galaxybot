use crate::osu::{
    scores::OsuScore,
    mods::OsuMods,
};

pub fn calculate_pp(score: &OsuScore) -> u32 {
    if score.mods.contains(OsuMods::Relax) || score.mods.contains(OsuMods::Relax2) || score.mods.contains(OsuMods::Autoplay) {
        return 0;
    } 

    let mut multiplier = 1.12;

    if score.mods.contains(OsuMods::NoFail) {
        multiplier *= 0.9f32.max(1.0 - 0.02 * score.count_miss as f32);
    }

    if score.mods.contains(OsuMods::SpunOut) {
        let total_hits = score.count_miss + score.count_50 + score.count_100 + score.count_300;
        multiplier *= 1.0 - (score.beatmap.count_spinner as f32 / total_hits as f32).powf(0.85);
    }

    (multiplier * ((calculate_aim(score).powf(1.1) +
        calculate_speed(score).powf(1.1) +
        calculate_acc(score).powf(1.1)).powf(1.0 / 1.1))).round() as u32
}

fn calculate_aim(score: &OsuScore) -> f32 {
    let mut aim = score.beatmap.aim;

    if score.mods.contains(OsuMods::TouchDevice) {
        aim = aim.powf(0.8);
    }

    aim = (5.0 * 1.0f32.max(aim / 0.0675) - 4.0).powi(3) / 100000.0;

    let total_hits = score.count_miss + score.count_50 + score.count_100 + score.count_300;

    let length_bonus = 0.95 + 0.4 * 1.0f32.min(total_hits as f32 / 2000.0)
        + if total_hits > 2000 { 0.5 * (total_hits as f32 / 2000.0).log10() } else { 0.0 };

    aim *= length_bonus;

    if score.count_miss > 0 {
        aim *= 0.97 * (1.0 - (score.count_miss as f32 / total_hits as f32).powf(0.775)).powi(score.count_miss as i32);
    }

    aim *= 1.0f32.min((score.max_combo as f32).powf(0.8) / (score.beatmap.max_combo as f32).powf(0.8));

    let mut ar_factor = 0.0;

    if score.beatmap.ar > 10.33 {
        ar_factor += 0.4 * (score.beatmap.ar - 10.33);
    }
    else if score.beatmap.ar < 8.0 {
        ar_factor += 0.01 * (8.0 - score.beatmap.ar);
    }

    aim *= 1.0 + ar_factor.min(ar_factor * total_hits as f32 / 1000.0);

    if score.mods.contains(OsuMods::Hidden) {
        aim *= 1.0 + 0.04 * (12.0 - score.beatmap.ar);
    }

    if score.mods.contains(OsuMods::Flashlight) {
        aim *= 1.0 + 0.35 * 1.0f32.min(total_hits as f32 / 200.0) +
            if total_hits > 200 { 0.3 * 1.0f32.min((total_hits - 200) as f32 / 300.0) + 
            if total_hits > 500 { (total_hits - 500) as f32 / 1200.0 } else { 0.0 } } else { 0.0 };
    }

    aim *= 0.5 + score.accuracy / 2.0;

    aim *= 0.98 + (score.beatmap.od.powi(2) / 2500.0);

    aim
}

fn calculate_speed(score: &OsuScore) -> f32 {
    let mut speed = (5.0 * 1.0f32.max(score.beatmap.speed / 0.0675) - 4.0).powi(3) / 100000.0;

    let total_hits = score.count_miss + score.count_50 + score.count_100 + score.count_300;

    speed *= 0.95 + 0.4 * 1.0f32.min(total_hits as f32 / 2000.0) +
        if total_hits > 2000 { 0.5 * (total_hits as f32 / 2000.0).log10() } else { 0.0 };

    if score.count_miss > 0 {
        speed *= 0.97 * (1.0 - (score.count_miss as f32 / total_hits as f32).powf(0.775)).powf((score.count_miss as f32).powf(0.875));
    }

    speed *= 1.0f32.min((score.max_combo as f32).powf(0.8) / (score.beatmap.max_combo as f32).powf(0.8));

    let mut ar_factor = 0.0;

    if score.beatmap.ar > 10.33 {
        ar_factor += 0.4 * (score.beatmap.ar - 10.33);
    }

    speed *= 1.0 + ar_factor.min(ar_factor * total_hits as f32 / 1000.0);

    if score.mods.contains(OsuMods::Hidden) {
        speed *= 1.0 + 0.04 * (12.0 - score.beatmap.ar);
    }

    speed *= (0.95 + score.beatmap.od.powi(2) / 750.0) *
        score.accuracy.powf((14.5 - score.beatmap.od.max(8.0)) / 2.0);

    speed *= 0.98f32.powf(if (score.count_50 as f32) < total_hits as f32 / 500.0 { 0.0 } 
        else {
            score.count_50 as f32 - total_hits as f32 / 500.0
        });

    speed
}

fn calculate_acc(score: &OsuScore) -> f32 {

    let total_hits = score.count_miss + score.count_50 + score.count_100 + score.count_300;

    let mut better_accuracy = ((score.count_300 as i32 - (total_hits as i32 - score.beatmap.count_normal as i32)) * 6 +
        score.count_100 as i32 * 2 + score.count_50 as i32) as f32 / (score.beatmap.count_normal * 6) as f32;

    if better_accuracy < 0.0 { better_accuracy = 0.0; }

    let mut acc = 1.52163f32.powf(score.beatmap.od) * better_accuracy.powi(24) * 2.83;

    acc *= 1.15f32.min((score.beatmap.count_normal as f32 / 1000.0).powf(0.3));

    if score.mods.contains(OsuMods::Hidden) {
        acc *= 1.08;
    }

    if score.mods.contains(OsuMods::Flashlight) {
        acc *= 1.02;
    }

    acc
}