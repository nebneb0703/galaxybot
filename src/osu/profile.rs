use serde_json::Value;
use serde_json::Result;

use serenity::model::id::ChannelId;
use serenity::utils::Colour;
use serenity::http::Http;

use num_format::{ Locale, ToFormattedString };

use std::sync::Arc;

use crate::osu::scores::OsuScore;
use crate::osu::mode::OsuMode;

pub(crate) const PROFILE_URL: &str = "https://osu.ppy.sh/users";
pub(crate) const PROFILE_PIC_URL: &str = "http://s.ppy.sh/a";

#[derive(Debug)]
pub struct OsuProfile {
    pub user_id: u32,
    pub username: String,
    pub country: String,
    pub rank: u32,
    pub country_rank: u32,
    pub pp: u32,
    pub ss: u32,
    pub ssh: u32,
    pub s: u32,
    pub sh: u32,
    pub a: u32,
}

impl OsuProfile {
    pub fn deserialise(json: String) -> Result<OsuProfile> {
        let mut values: Value = serde_json::from_str(json.as_str())?;
        values = values[0].clone();

        let mut user_id: u32 = 0;
        let mut username: String = String::new();
        let mut country: String = String::new();
        let mut rank: u32 = 0;
        let mut country_rank: u32 = 0;
        let mut pp: u32 = 0;
        let mut ss: u32 = 0;
        let mut ssh: u32 = 0;
        let mut s: u32 = 0;
        let mut sh: u32 = 0;
        let mut a: u32 = 0;

        if let Value::String(s) = &values["user_id"] {
            user_id = s.parse::<u32>().unwrap();
        }

        if let Value::String(s) = &values["username"] {
            username = s.clone();
        }

        if let Value::String(s) = &values["country"] {
            country = s.clone();
        }

        if let Value::String(s) = &values["pp_rank"] {
            rank = s.parse::<u32>().unwrap();
        }

        if let Value::String(s) = &values["pp_country_rank"] {
            country_rank = s.parse::<u32>().unwrap();
        }

        if let Value::String(s) = &values["pp_raw"] {
            pp = s.parse::<f32>().unwrap().ceil() as u32;
        }

        if let Value::String(s) = &values["count_rank_ss"] {
            ss = s.parse::<u32>().unwrap();
        }

        if let Value::String(s) = &values["count_rank_ssh"] {
            ssh = s.parse::<u32>().unwrap();
        }
        
        if let Value::String(st) = &values["count_rank_s"] {
            s = st.parse::<u32>().unwrap();
        }

        if let Value::String(s) = &values["count_rank_sh"] {
            sh = s.parse::<u32>().unwrap();
        }
        
        if let Value::String(s) = &values["count_rank_a"] {
            a = s.parse::<u32>().unwrap();
        }

        Ok(OsuProfile { user_id, username, country, rank, country_rank, pp, ss, ssh, s, sh, a })
    }

    pub async fn embed_with_best(&self, http: Arc<Http>, channel_id: ChannelId, mode: OsuMode, best: &Vec<OsuScore>) -> serenity::Result<()> {
        let mut best_message: String = String::from("```\n");

        for score in best {
            best_message += format!("({}) {} - {} [{}] -- {} {}* {:2.2}% {}pp {}\n\n", 
                score.beatmap.creator, score.beatmap.artist, score.beatmap.title,
                score.beatmap.diff_name, score.rank.name(),
                score.beatmap.star_rating, score.accuracy * 100.0, score.pp.to_formatted_string(&Locale::en_GB),
                score.mods.short_list()).as_str();
        }

        best_message.push_str("```");

        let emoji = mode.emoji(http.clone()).await;

        channel_id.send_message(&http, |m| m.embed(|e| 
            e
            .thumbnail(format!("{}/{}", PROFILE_PIC_URL, self.user_id))
            .colour(Colour::from_rgb(228, 132, 250))
            .title(format!("{} [{}] {}", emoji, mode.name(), self.username.clone()))

            .field("Country", self.flag(), true)
            .field("Rank", self.rank.to_formatted_string(&Locale::en_GB), true)
            .field("Country Rank", self.country_rank.to_formatted_string(&Locale::en_GB), true)

                .field("SS", self.ss, true)
                .field("SS+", self.ssh, true)
                .field("S+", self.s, true)
                .field("S", self.s, true)
                .field("A", self.a, true)
                .field("PP", self.pp.to_formatted_string(&Locale::en_GB), true)
                .field("Top Scores", best_message, false)

            .url(format!("{}/{}/{}", PROFILE_URL, self.user_id, mode.url()))
        )).await?;

        Ok(())
    }

    pub fn flag(&self) -> String {
        format!(":flag_{}:", self.country.to_lowercase())
    }
}