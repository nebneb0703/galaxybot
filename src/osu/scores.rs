use serde_json::Value;
use serde_json::Result;

use serenity::model::id::ChannelId;
use serenity::utils::Colour;
use serenity::http::Http;

use num_format::{ Locale, ToFormattedString };

use std::sync::Arc;

use crate::osu::*;
use crate::osu::pp::*;

pub(crate) const BEATMAPSET_THUMBNAIL_START: &str = "https://b.ppy.sh/thumb/";
pub(crate) const BEATMAPSET_THUMBNAIL_END: &str = "l.jpg";
pub(crate) const BEATMAP_URL: &str = "https://osu.ppy.sh/beatmapsets";

use chrono::prelude::*;

#[derive(Clone, Debug)]
pub struct OsuScore {
    pub beatmap_id: u32,
    pub beatmap: OsuBeatmap,
    pub mode: OsuMode,
    pub score_id: u32,
    pub user_id: u32,
    pub score: u32,
    pub accuracy: f32,
    pub max_combo: u32,
    pub count_300: u32,
    pub count_300p: u32, //geki
    pub count_100: u32,
    pub count_100p: u32, //katu
    pub count_50: u32,
    pub count_miss: u32,
    pub fc: bool,
    pub mods: OsuMods,
    pub rank: OsuGrade,
    pub pp: u32,
    pub date: DateTime<Utc>,
}

impl OsuScore {
    pub async fn deserialise(json: String, osu: &Osu, mode: OsuMode) -> Result<Vec<OsuScore>> {
        let values: Value = serde_json::from_str(json.as_str())?;
        
        let mut vec: Vec<OsuScore> = Vec::new();

        if let Value::Array(a) = values {
            for val in a {
                let mut beatmap_id: u32 = 0;
                let mut score_id: u32 = 0;
                let mut user_id: u32 = 0;
                let mut score: u32 = 0;
                let mut max_combo: u32 = 0;
                let mut count_300: u32 = 0;
                let mut count_300p: u32 = 0; //geki
                let mut count_100: u32 = 0;
                let mut count_100p: u32 = 0; //katu
                let mut count_50: u32 = 0;
                let mut count_miss: u32 = 0;
                let mut fc: bool = false;
                let mut mods: OsuMods = OsuMods::empty();
                let mut rank: OsuGrade = OsuGrade::Fail;
                let mut pp: u32 = 0;
                let mut date: DateTime<Utc> = chrono::MIN_DATETIME;

                if let Value::String(s) = &val["beatmap_id"] {
                    beatmap_id = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["enabled_mods"] {
                    mods = OsuMods::from_bits(s.parse::<u32>().unwrap()).unwrap();
                }

                let beatmap: OsuBeatmap = osu.get_map_convert(beatmap_id, mods, mode).await.expect("Failed to get beatmap");

                if let Value::String(s) = &val["score_id"] {
                    score_id = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["user_id"] {
                    user_id = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["score"] {
                    score = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["maxcombo"] {
                    max_combo = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["count300"] {
                    count_300 = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["countgeki"] {
                    count_300p = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["count100"] {
                    count_100 = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["countkatu"] {
                    count_100p = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["count50"] {
                    count_50 = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["countmiss"] {
                    count_miss = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["perfect"] {
                    fc = if s.parse::<u8>().unwrap() == 1 { true } else { false };
                }

                if let Value::String(s) = &val["rank"] {
                    rank = OsuGrade::from(&s.clone()).unwrap();
                }

                if let Value::String(s) = &val["pp"] {
                    pp = s.parse::<f32>().unwrap().round() as u32;
                }

                if let Value::String(s) = &val["date"] {
                    match Utc.datetime_from_str(s, "%Y-%m-%d %H:%M:%S") {
                        Ok(d) => date = d,
                        Err(e) => error!("{}", e),
                    }
                }

                let mut temp = OsuScore { beatmap_id, beatmap, mode, score_id, user_id, score, accuracy: 0.0, max_combo, count_300,
                    count_300p, count_100, count_100p, count_50, count_miss, fc,
                    mods, rank, pp, date };

                temp.calculate_accuracy();
                temp.apply_mods();

                if pp == 0 {
                    temp.calculate_pp();
                }

                vec.push(temp);
            }
        }

        Ok(vec)
    }

    pub fn default_from_map(beatmap: OsuBeatmap, mode: OsuMode, user_id: u32) -> Self {
        OsuScore {
            beatmap_id: beatmap.beatmap_id,
            beatmap: beatmap.clone(),
            mode,
            score_id: 0,
            user_id,
            score: 0,
            accuracy: 0.0,
            max_combo: 0,
            count_300: 0,
            count_300p: 0,
            count_100: 0,
            count_100p: 0,
            count_50: 0,
            count_miss: beatmap.max_combo,
            fc: false,
            mods: OsuMods::empty(),
            rank: OsuGrade::Fail,
            pp: 0,
            date: Utc::now(),
        }
    }

    pub fn compare(&self, presence: OsuPresence) -> bool {

        let small_text = match presence.song {
            Some(s) => s,
            None => return false,
        };

        let score_text = format!("{} - {} [{}]", self.beatmap.artist, self.beatmap.title, self.beatmap.diff_name);

        let equal = score_text == small_text;

        let after = self.date > presence.time;

        //debug!("{} == {} = {}; {} > {} = {}", score_text, small_text, equal, self.date, presence.time, after);

        return equal && after;
    }

    pub async fn embed(&self, http: Arc<Http>, channel_id: ChannelId, user: OsuProfile) -> serenity::Result<()> {

        let max_combo_text = if self.fc { " [FC]".to_owned() } else {
             if self.beatmap.max_combo == 0 { "".to_owned() } else { format!("/{}", self.beatmap.max_combo) } 
        };

        let emoji = self.mode.emoji(http.clone()).await;

        channel_id.send_message(&http, |m| 
            m.embed(|e| {
                e
                .thumbnail(format!("{}{}{}", BEATMAPSET_THUMBNAIL_START, self.beatmap.beatmapset_id, BEATMAPSET_THUMBNAIL_END))
                .colour(Colour::from_rgb(228, 132, 250))
                .title(format!("{} [{}] ({}) {} - {} [{}]", emoji, self.mode.name(), self.beatmap.creator.clone(), self.beatmap.artist.clone(),
                 self.beatmap.title.clone(), self.beatmap.diff_name.clone()))
        
                .description(format!("{}```Score: {}    {}*    {:.2}%    {}``` ```CS:{:.1}    AR:{:.1}    OD:{:.1}    HP:{:.1}    BPM:{:.0}```",
                    if self.beatmap.convert { "[Convert]\n" } else { "" },
                    self.score.to_formatted_string(&Locale::en_GB), self.beatmap.star_rating,
                    self.accuracy * 100.0, self.mods.short_list(), self.beatmap.cs, self.beatmap.ar,
                    self.beatmap.od, self.beatmap.hp, self.beatmap.bpm))
    
                .author(|a| a
                    .name(format!("{}", user.username))
                    .url(format!("{}/{}/{}", PROFILE_URL, self.user_id, self.mode.url()))
                    .icon_url(format!("{}/{}", PROFILE_PIC_URL, self.user_id))
                )
    
                .field("Rank", self.rank.clone().name(), true)
                .field("PP", self.pp, true)
                .field("Max Combo", format!("{}{}", self.max_combo, max_combo_text), true);
    
                match self.mode {
                    OsuMode::Standard => { e
                        .field("300", self.count_300, true)
                        .field("300+", self.count_300p, true)
                        .field("100", self.count_100, true)
                        .field("100+", self.count_100p, true)
                        .field("50", self.count_50, true)
                        .field("Miss", self.count_miss, true)
                    }
    
                    OsuMode::Mania => { e
                        .field("300+", self.count_300p, true)
                        .field("300", self.count_300, true)
                        .field("200", self.count_100p, true)
                        .field("100", self.count_100, true)
                        .field("50", self.count_50, true)
                        .field("Miss", self.count_miss, true)
                    }
    
                    OsuMode::Taiko => { e
                        .field("Great", self.count_300, true)
                        .field("Good", self.count_100, true)
                        .field("Miss", self.count_miss, true)
                    }
    
                    OsuMode::CatchTheBeat => { e
                        .field("Fruits", self.count_300, true)
                        .field("Ticks", self.count_100, true)
                        .field("Droplets", self.count_50, true)
                        .field("Droplets Miss", self.count_100p, true)
                        .field("Miss", self.count_miss, true)
                    }
                }
                
        
                .url(format!("{}/{}#{}/{}", BEATMAP_URL, self.beatmap.beatmapset_id, self.mode.url(), self.beatmap_id))
                
                .timestamp(self.date.to_rfc3339())
            })
        ).await?;

        Ok(())
    }

    pub fn calculate_accuracy(&mut self) {
        self.accuracy = match self.mode {
            OsuMode::Standard => {
                ( 6 * self.count_300
                + 2 * self.count_100
                + self.count_50 ) as f32 / (6 * (self.count_300 + self.count_100 + self.count_50 + self.count_miss)) as f32
            }

            OsuMode::Mania => {
                ( 6 * (self.count_300 + self.count_300p)
                + 4 * self.count_100p 
                + 2 * self.count_100
                + self.count_50 ) as f32 / (6 * (self.count_300 + self.count_300p + self.count_100 + self.count_100p + self.count_50 + self.count_miss)) as f32
            }

            OsuMode::Taiko => {
                ( 2 * self.count_300
                + self.count_100) as f32 / (2 * (self.count_300 + self.count_100 + self.count_miss)) as f32
            }

            OsuMode::CatchTheBeat => {
                ( self.count_300 + self.count_100 + self.count_50 ) as f32 
                / ( self.count_300 + self.count_100 + self.count_50 + self.count_100p + self.count_miss ) as f32
            }
        };
    }

    pub fn calculate_pp(&mut self) {
        self.pp = match self.mode {
            OsuMode::Standard => standard::calculate_pp(&self),
            OsuMode::Mania => mania::calculate_pp(&self),
            OsuMode::Taiko => taiko::calculate_pp(&self),
            _ => 0,
        };
    }

    pub async fn update_star(&mut self, osu: &Osu) {
        self.beatmap = osu.get_map_convert(self.beatmap_id, self.mods, self.mode).await.expect("Failed to get beatmap");
    }

    pub fn apply_mods(&mut self) {
        let mods = self.mods.difficulty_increasing();
        if mods.is_empty() {
            return;
        }

        let mut speed = 1.0;

        if mods.contains(OsuMods::DoubleTime) {
            speed *= 1.5;
        }

        else if mods.contains(OsuMods::HalfTime) {
            speed *= 0.75;
        }

        self.beatmap.bpm *= speed;

        match self.mode {
            OsuMode::Standard => {}
            _ => {
                // Unsupported for other gamemodes.
                return;
            }
        }

        // Idk what this is, it's copied from 4 year old python code. ¯\_(ツ)_/¯
        let od0_ms = 79.5;
		let od10_ms = 19.5;
		let ar0_ms = 1800.0;
		let ar5_ms = 1200.0;
        let ar10_ms = 450.0;
        
		let od_ms_step = 6.0;
		let ar_ms_step1 = 120.0;
        let ar_ms_step2 = 150.0;

        let mut od_ar_hp_multiplier = 1.0;
        let mut cs_multiplier = 1.0;

        if mods.contains(OsuMods::HardRock) {
            od_ar_hp_multiplier *= 1.4;
            cs_multiplier *= 1.3;
        }

        else if mods.contains(OsuMods::Easy) {
            od_ar_hp_multiplier *= 0.5;
            cs_multiplier *= 0.5;
        }

        self.beatmap.hp = 10.0f32.min(self.beatmap.hp * od_ar_hp_multiplier);

        self.beatmap.cs *= cs_multiplier;
        self.beatmap.cs = 0.0f32.max(10.0f32.min(self.beatmap.cs));

        self.beatmap.od *= od_ar_hp_multiplier;
        self.beatmap.ar *= od_ar_hp_multiplier;

        let mut odms = od0_ms - (od_ms_step * self.beatmap.od).ceil();   
        odms = od0_ms.min(odms.max(od10_ms));
        odms /= speed;
        self.beatmap.od = (od0_ms - odms) / od_ms_step;

        let mut arms = if self.beatmap.ar <= 5.0 { ar0_ms - ar_ms_step1 * self.beatmap.ar }
            else { ar5_ms - ar_ms_step2 * (self.beatmap.ar - 5.0)};
        arms = ar0_ms.min(arms.max(ar10_ms));
        arms /= speed;
        self.beatmap.ar = if self.beatmap.ar <= 5.0 { (ar0_ms - arms) / ar_ms_step1 } else { 5.0 + (ar5_ms - arms) / ar_ms_step2 };
    }

    pub fn force_fc_accuracy(&mut self, accuracy: f32) {
        match self.mode {
            OsuMode::Standard => {
                let mut hit300 = 0;
                let mut hit100 = 0;
                let mut hit50 = 0;
        
                let mut note_count = (self.beatmap.count_normal + self.beatmap.count_slider + self.beatmap.count_spinner) as f32;
                let mut total = (accuracy * note_count).floor() * 300.0;
        
                while total > 0.0 {
                    let temp = total / note_count;
        
                    if temp > 100.0 {
                        hit300 += 1;
                        total -= 300.0;
                    }
                    else if temp > 50.0 {
                        hit100 += 1;
                        total -= 100.0;
                    }
                    else {
                        hit50 += 1;
                        total -= 50.0;
                    }
        
                    note_count -= 1.0;
                }

                self.count_300 = hit300;
                self.count_300p = 0;
                self.count_100 = hit100;
                self.count_100p = 0;
                self.count_50 = hit50;
                self.count_miss = 0;

                self.max_combo = self.beatmap.max_combo;
                self.fc = true;

                self.calculate_accuracy();
            },
            OsuMode::Taiko => {
                let mut hit300 = 0;
                let mut hit100 = 0;

                let mut total = (accuracy * self.beatmap.max_combo as f32).floor() as u32 * 2;
                let mut note_count = self.beatmap.max_combo;

                while total > 0 {
                    let temp = total as f32 / note_count as f32;
        
                    if temp > 1.0 {
                        hit300 += 1;
                        total -= 2;
                    }
                    else {
                        hit100 += 1;
                        total -= 1;
                    }
        
                    note_count -= 1;
                }

                self.count_300 = hit300;
                self.count_100 = hit100;
                self.count_miss = 0;

                self.max_combo = self.beatmap.max_combo;
                self.fc = true;

                self.calculate_accuracy();
            }
            _ => {
                // Currently unsupported for other gamemodes
                error!("Force accuracy for {} is unsupported.", self.mode.name());
            }
        }
    }
}
