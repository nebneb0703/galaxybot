use serenity::{

    framework::standard::{
        Args,
        CommandResult,
        macros::{
            command,
            group,
        }
    },
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use crate::db::*;

use diesel::SaveChangesDsl;

#[group("Admin Config")]
#[prefix("config")]
#[required_permissions("MANAGE_CHANNELS")]
#[commands(set_auto_channel)]
struct AdminConfig;

#[command("auto")]
#[only_in(guilds)]
#[usage("o!config auto <channel>")]
#[description("Sets the channel to post recents automatically.")]
pub async fn set_auto_channel(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let guild_id = *msg.guild_id.unwrap().as_u64();

    let mut guild = match database.get_guild(guild_id).await? {
        Some(g) => g,
        None => {
            let default = DbGuild::new(guild_id);
            database.add_guild(default.clone()).await?;

            default
        }
    };

    let arg = match args.current() {
        Some(a) => a,
        None => {
            if guild.auto_channel_id.0 == 0 {
                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Auto")
                        .description("Auto is currently not configured in this server.")
                        .colour(Colour::from_rgb(228, 132, 250))
                    })
                }).await?;
            }
            else {
                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Auto")
                        .description(format!("Current auto channel: <#{}>", guild.auto_channel_id.0))
                        .colour(Colour::from_rgb(228, 132, 250))
                    })
                }).await?;
            }

            return Ok(());
        }
    };
    
    let channel_vec = nebbot_utils::discord::parse_channels(arg.to_owned());

    let channel = match channel_vec.first() {
        Some(c) => c,
        None => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Auto")
                    .description("Invalid channel argument")
                    .colour(Colour::from_rgb(228, 132, 250))
                })
            }).await?;

            return Ok(());
        }
    };

    guild.auto_channel_id = nebbot_utils::types::TextU64(*channel);

    let _: DbGuild = guild.save_changes(&*database.conn.lock().await)?;

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Auto")
            .description(format!("Successfully updated auto channel to <#{}>", *channel))
            .colour(Colour::from_rgb(228, 132, 250))
        })
    }).await?;

    Ok(())
}
