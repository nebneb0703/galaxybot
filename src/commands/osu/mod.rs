use serenity::framework::standard::{
    Args,
    macros::group
};

use crate::osu::*;
use crate::db::Db;

mod user;
pub use user::*;

mod best;
pub use best::*;

mod latest;
pub use latest::*;

mod sync;
pub use sync::*;

mod unsync;
pub use unsync::*;

mod set_mode;
pub use set_mode::*;

mod auto;
pub use auto::*;

mod unauto;
pub use unauto::*;

mod fc;
pub use fc::*;

#[group("Osu")]
#[commands(user, best, latest, sync, unsync, set_mode, auto, unauto, fc)]
struct OsuCommands;

#[derive(Debug)]
pub enum ParsingError {
    DatabaseError(diesel::result::Error),
    OsuError(reqwest::Error),
}

impl std::fmt::Display for ParsingError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ParsingError::DatabaseError(e) =>
                write!(f, "Parse Args Database Error: {}", e),
            ParsingError::OsuError(e) =>
                write!(f, "Parse Args Osu Error: {}", e),
        }
    }
}

impl std::error::Error for ParsingError { }

impl From<diesel::result::Error> for ParsingError {
    fn from(f: diesel::result::Error) -> Self {
        ParsingError::DatabaseError(f)
    }
}

impl From<reqwest::Error> for ParsingError {
    fn from(f: reqwest::Error) -> Self {
        ParsingError::OsuError(f)
    }
}

pub enum ParseMode {
    Default(OsuMode),
    Explicit(OsuMode),
}

impl ParseMode {
    pub fn mode(&self) -> OsuMode {
        match self {
            ParseMode::Default(m) => m.clone(),
            ParseMode::Explicit(m) => m.clone(),
        }
    }
}

async fn parse_args(mut args: Args, osu: &Osu, db: &Db, id: u64) -> Result<Option<(OsuAccount, OsuMode)>, ParsingError> {
    let mut account: OsuAccount;
    let mode: OsuMode;

    match db.get_user(id).await? {
        Some(user) => {
            // Assume current user
            account = OsuAccount::from(user.osu_username.as_str());
    
            // Check for mode
            mode = match args.current() {
                Some(a) => match OsuMode::parse(a) {
                    Some(m) => m,
                    None => { 
                        // If the first argument is not a mode, check for user
                        let arg = match args.current() {
                            Some(a) => a,
                            None => { return Ok(None); }
                        };
                
                        account = match OsuAccount::from_dc(db, arg).await? {
                            Some(a) => a,
                            None =>  { return Ok(None); }
                        };
                
                        // Check for mode
                        match args.advance().current() {
                            Some(a) => match OsuMode::parse(a) {
                                Some(m) => m,
                                None => OsuMode::Standard
                            },
                            None => {
                                match db.get_user_by_osu(osu, account.clone()).await? {
                                    Some(i) => i.default_mode,
                                    None => OsuMode::Standard,
                                }
                            }
                        }
                    },
                },
                None => user.default_mode,
            };
        }
        None => {
            {
                // Check for user
                let arg = match args.current() {
                    Some(a) => a,
                    None => { return Ok(None); }
                };
        
                account = match OsuAccount::from_dc(db, arg).await? {
                    Some(a) => a,
                    None =>  { return Ok(None); }
                };
        
                // Check for mode
                mode = match args.advance().current() {
                    Some(a) => match OsuMode::parse(a) {
                        Some(m) => m,
                        None => OsuMode::Standard
                    },
                    None => {
                        match db.get_user_by_osu(osu, account.clone()).await? {
                            Some(i) => i.default_mode,
                            None => OsuMode::Standard,
                        }
                    }
                };
            }
        }
    }

    Ok(Some((account, mode)))
}

async fn parse_mode_remainder(mut args: Args, db: &Db, id: u64) -> Result<Option<(OsuAccount, ParseMode, Args)>, ParsingError> {
    let account: OsuAccount;
    let mode: ParseMode;

    match db.get_user(id).await? {
        Some(user) => {
            // Get current user
            account = OsuAccount::from(user.osu_username.as_str());
    
            // Check for mode
            mode = match args.current() {
                Some(a) => match OsuMode::parse(a) {
                    Some(m) => {
                        args.advance();
                        ParseMode::Explicit(m)
                    },
                    None => ParseMode::Default(user.default_mode),
                },
                None => ParseMode::Default(user.default_mode),
            };
        }
        None => return Ok(None),
    }

    Ok(Some((account, mode, args)))
}