use serenity::framework::standard::{
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

#[command]
#[description("Unsyncs osu! account to the bot (in case you accidentally synced to the wrong account).")]
async fn unsync(ctx: &Context, msg: &Message) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    match database.get_user(*msg.author.id.as_u64()).await? {
        Some(_) => {},
        None => {
            msg.channel_id.say(&ctx.http, "You have not already synced discord with osu!.").await?;
            return Ok(()); 
        },
    };

    database.remove_user(*msg.author.id.as_u64()).await?;

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title(format!("Unsynced {}", msg.author.tag()))
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    
    )).await?;

    Ok(())
}