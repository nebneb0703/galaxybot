use serenity::framework::standard::{
    CommandResult,
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use crate::db::*;

use diesel::SaveChangesDsl;

#[command]
#[only_in(guilds)]
#[description("Sets the server to post recent scores to.")]
async fn auto(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let guild = match database.get_guild(guild_id).await? {
        Some(g) => {
            if g.auto_channel_id.0 == 0 {
                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Auto")
                        .description("Auto is currently not configured in this server.")
                        .colour(Colour::from_rgb(228, 132, 250))
                    })
                }).await?;

                return Ok(());
            }

            g
        },
        None => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Auto")
                    .description("Auto is currently not configured in this server.")
                    .colour(Colour::from_rgb(228, 132, 250))
                })
            }).await?;

            return Ok(());
        }
    };

    let mut user = match database.get_user(discord_id).await? {
        Some(u) => u,
        None => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Auto")
                    .description("You have not synced discord with osu!.")
                    .colour(Colour::from_rgb(228, 132, 250))
                })
            }).await?;

            return Ok(());
        }
    };

    user.auto_guild_id = guild.guild_id.clone();

    let _: DbUser = user.save_changes(&*database.conn.lock().await)?;

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Auto")
            .description(format!("Successfully updated auto channel to <#{}>", guild.auto_channel_id.0))
            .colour(Colour::from_rgb(228, 132, 250))
        })
    }).await?;

    Ok(())
}