use serenity::framework::standard::{
    Args,
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use crate::osu::*;

use crate::db::DbUser;

#[command]
#[description("Syncs osu! account to the bot")]
#[usage("o!sync [osu-username]")]
async fn sync(ctx: &Context, msg: &Message, mut args: Args) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let data = ctx.data.read().await;
    let osu = data.get::<crate::OsuContainer>().expect("Couldn't retrieve OsuContainer");
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let arg = match args.current() {
        Some(a) => a,
        None => { 
            msg.channel_id.say(&ctx.http, "Provide a valid user argument").await?;
            return Ok(());
        }
    };

    let account = OsuAccount::from(arg);

    let mode = match args.advance().current() {
        Some(a) => match OsuMode::parse(a) {
            Some(m) => m,
            None => OsuMode::Standard
        },
        None => OsuMode::Standard
    };

    match database.get_user(*msg.author.id.as_u64()).await? {
        Some(_) => {
            msg.channel_id.say(&ctx.http, "Already synced discord with osu!.").await?;
            return Ok(()); 
        }
        _ => {},
    }

    let profile = osu.get_user(account.clone(), mode).await?;

    let user = DbUser::default(*msg.author.id.as_u64(), profile.user_id,
        profile.username.clone(), mode.clone());

    database.add_user(user.clone()).await?;

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title(format!("Synced {} with user {}", msg.author.tag(), user.osu_username))

        .thumbnail(format!("{}/{}", PROFILE_PIC_URL, user.osu_id.0))
        .colour(Colour::from_rgb(228, 132, 250))
        .url(format!("{}/{}/{}", PROFILE_URL, user.osu_id.0, mode.url()))
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    
    )).await?;

    Ok(())
}