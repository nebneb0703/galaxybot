use serenity::framework::standard::{
    CommandResult,
    Args,
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

#[command]
#[aliases("b", "top", "t")]
#[description("Displays best score for mode.")]
#[usage("o!best [user] [mode]")]
async fn best(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let osu = data.get::<crate::OsuContainer>().expect("Couldn't retrieve OsuContainer");
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let (account, mode) = match super::parse_args(args, &osu, &database, *msg.author.id.as_u64()).await? {
        Some((a,m)) => (a,m),
        None => {
            msg.channel_id.say(&ctx.http, "Provide a valid user argument").await?;
            return Ok(());
        }
    };

    let profile = osu.get_user(account.clone(), mode).await?;

    let _scores = osu.get_best(account.clone(), mode, 5).await?;

    let score = match _scores.first() {
        Some(s) => s,
        None => {
            msg.channel_id.say(&ctx.http, "No best score found.").await?;
            return Ok(());
        }
    };

    score.embed(ctx.http.clone(), msg.channel_id, profile).await?;

    Ok(())
}