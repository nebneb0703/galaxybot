use serenity::framework::standard::{
    CommandResult,
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use crate::db::*;

use diesel::SaveChangesDsl;

use nebbot_utils::types::TextU64;

#[command]
#[only_in(guilds)]
#[description("Disables auto recent feature completely.")]
async fn unauto(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let mut user = match database.get_user(*msg.author.id.as_u64()).await? {
        Some(u) => u,
        None => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Auto")
                    .description("You have not already synced discord with osu!.")
                    .colour(Colour::from_rgb(228, 132, 250))
                })
            }).await?;

            return Ok(());
        }
    };

    user.auto_guild_id = TextU64(0);

    let _: DbUser = user.save_changes(&*database.conn.lock().await)?;

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Auto")
            .description("Successfully disabled auto.")
            .colour(Colour::from_rgb(228, 132, 250))
        })
    }).await?;

    Ok(())
}