use serenity::framework::standard::{
    CommandResult,
    Args,
    macros::command
};

use num_format::{ Locale, ToFormattedString };

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use regex::Regex;

use crate::osu::*;

lazy_static::lazy_static! {
    static ref ARGS_REGEX: Regex = Regex::new(
        r"(?xi) (?:
        \s*(?P<accuracy>(?:\d{1,2}%|\d{1,2}.\d{1,2}%))|
        \s*(?P<add_mods>\+(\w{1,2})+)|
        \s*(?P<take_mods>\-(\w{1,2})+)|
        \s*(?P<score>(?:1m|\d{1,3}k))
        ){1,4}").unwrap();

    static ref MAP_REGEX: Regex = Regex::new(
        r"(?:<?)https://osu\.ppy\.sh/beatmapsets/(?P<beatmapset_id>\d*)#(?P<mode>\w*)/(?P<beatmap_id>\d*)(?:>?)"
    ).unwrap();
}

const MANIA_SCORES: [(&'static str, u32); 5] = [
    ("1m", 1_000_000),
    ("950k", 950_000),
    ("900k", 900_000),
    ("850k", 850_000),
    ("800k", 800_000)
];

const MANIA_SCORES_HALF: [(&'static str, u32); 5] = [
    ("500k", 500_000),
    ("450k", 450_000),
    ("400k", 400_000),
    ("350k", 350_000),
    ("300k", 300_000)
];

const MANIA_SCORES_QUARTER: [(&'static str, u32); 5] = [
    ("250k", 250_000),
    ("200k", 200_000),
    ("150k", 150_000),
    ("100k", 100_000),
    ("50k", 50_000)
];

const MANIA_SCORES_EIGHTH: [(&'static str, u32); 5] = [
    ("125k", 125_000),
    ("100k", 100_000),
    ("75k", 75_000),
    ("50k", 50_000),
    ("25k", 25_000)
];

#[command]
#[aliases("fullcombo")]
#[description("Displays potential FC scores for recent map.")]
#[usage("o!fc [map] [mode] [accuracy%] [+mods] [-mods] [scorek]")]
async fn fc(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let osu = data.get::<crate::OsuContainer>().expect("Couldn't retrieve OsuContainer");
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");
    
    let linked_map = args.current().map(|a| MAP_REGEX.captures(a).map(|c| 
        (c.name("mode").map(|m| OsuMode::parse(m.as_str())).flatten(),
        c.name("beatmap_id").map(|m| m.as_str().parse::<u32>().ok()).flatten())
    )).flatten().map(|m| if let Some(mode) = m.0 { if let Some(id) = m.1 { Some((mode, id)) } else { None } } else { None });

    if linked_map.is_some() {
        args.advance();
    }

    let (account, parse_mode, remainder) = match super::parse_mode_remainder(args, &database, *msg.author.id.as_u64()).await? {
        Some((a,m,r)) => (a,m,r),
        None => {
            msg.channel_id.say(&ctx.http, "Provide valid arguments").await?;
            return Ok(());
        }
    };

    let mut mode = parse_mode.mode();

    let profile = osu.get_user(account.clone(), mode).await?;
    let mut score;
    let old_score;

    let is_score_recent;
    let mut is_default = false;

    match linked_map {
        Some(Some((new_mode, id))) => {
            if let super::ParseMode::Default(_) = parse_mode {
                mode = new_mode;
            }

            let pb = osu.get_pb_score(account, id, mode).await?;
            old_score = pb.clone();

            match pb {
                Some(s) => score = s,
                None => {
                    let beatmap = osu.get_map_convert(id, OsuMods::empty(), mode).await?;

                    score = OsuScore::default_from_map(beatmap, mode, profile.user_id);
                    is_default = true;
                }
            }

            is_score_recent = false;
        }
        Some(None) => {
            msg.channel_id.say(&ctx.http, "Invalid link.").await?;
            return Ok(());
        }
        None => {
            let scores = osu.get_recent(account, mode, 1).await?;
        
            score = match scores.first() {
                Some(s) => s.clone(),
                None => {
                    msg.channel_id.say(&ctx.http, "No recent score found.").await?;
                    return Ok(());
                }
            };
        
            old_score = Some(score.clone());

            is_score_recent = true;
        }
    }    

    let is_fc = match score.mode {
        OsuMode::Standard => score.fc && score.accuracy == 1.0,
        OsuMode::Mania => score.score == 1_000_000,
        OsuMode::Taiko => score.fc && score.accuracy == 1.0,
        OsuMode::CatchTheBeat => score.fc && score.accuracy == 1.0,
    };

    match remainder.remains() {
        Some(r) => {
            let params = r.to_owned().clone();
            let captures = match ARGS_REGEX.captures(r) {
                Some(c) => c,
                None => {
                    if is_score_recent && is_fc {
                        msg.channel_id.say(&ctx.http, "Score is already an FC.").await?;
                            return Ok(());
                    }

                    default_fc(ctx, msg, profile, old_score, score).await?;
                    
                    return Ok(());
                }
            };

            match captures.name("accuracy").map(|c| {
                let string = c.as_str().to_owned();
                string[..string.len() - 1].parse::<f32>().unwrap() / 100.0 // SAFETY: Regex should ensure that this is always a valid f32.
            }) {
                Some(accuracy) => {
                    match mode {
                        OsuMode::Standard | OsuMode::Taiko => {},
                        _ => {
                            msg.channel_id.say(&ctx.http, format!("Custom accuracy not supported for {}.", mode.name())).await?;
                            return Ok(());
                        }
                    }

                    score.force_fc_accuracy(accuracy);
                }
                None => {
                    match mode {
                        OsuMode::Standard | OsuMode::Taiko => score.force_fc_accuracy(score.accuracy),
                        _ => {}
                    }
                },
            }

            match captures.name("add_mods").map(|c| {
                let string = c.as_str()[1..].replace(",","");
                string.as_bytes().chunks(2)
                .map(|s| unsafe { std::str::from_utf8_unchecked(s).to_owned() }) // SAFETY: Data originates as string, no need to check or validate.
                .collect::<Vec<_>>()
            }) {
                Some(modlist) => {
                    let mut mods = score.mods;
                    for modifier in modlist {
                        let current = match OsuMods::parse_short(modifier.clone()) {
                            Some(m) => m,
                            None => {
                                msg.channel_id.say(&ctx.http, format!("Invalid mod argument: '{}'.", modifier)).await?;
                                return Ok(());
                            }
                        };

                        mods.insert(current);
                    }

                    if !mods.valid() {
                        msg.channel_id.say(&ctx.http, format!("Selected mods are invalid: {}.", mods.short_list())).await?;
                        return Ok(());
                    }

                    score.mods = mods;
                    score.update_star(&osu).await;
                    score.apply_mods();
                }
                None => {},
            }

            match captures.name("take_mods").map(|c| {
                let string = c.as_str()[1..].replace(",","");
                string.as_bytes().chunks(2)
                .map(|s| unsafe { std::str::from_utf8_unchecked(s).to_owned() }) // SAFETY: Data originates as string, no need to check or validate.
                .collect::<Vec<_>>()
            }) {
                Some(modlist) => {
                    let mut mods = score.mods;
                    for modifier in modlist {
                        let current = match OsuMods::parse_short(modifier.clone()) {
                            Some(m) => m,
                            None => {
                                msg.channel_id.say(&ctx.http, format!("Invalid mod argument: '{}'.", modifier)).await?;
                                return Ok(());
                            }
                        };

                        mods.remove(current);
                    }

                    if !mods.valid() {
                        msg.channel_id.say(&ctx.http, format!("Selected mods are invalid: {}.", mods.short_list())).await?;
                        return Ok(());
                    }

                    score.mods = mods;
                    score.update_star(&osu).await;
                    score.apply_mods();
                }
                None => {},
            }

            match captures.name("score").map(|c| c.as_str().to_lowercase()) {
                Some(value) => {
                    match mode {
                        OsuMode::Mania => {},
                        _ => {
                            msg.channel_id.say(&ctx.http, format!("Custom score not supported for {}.", mode.name())).await?;
                            return Ok(());
                        }
                    }

                    let score_amount = score.mods.contains(OsuMods::HalfTime) as u32 +
                        score.mods.contains(OsuMods::Easy) as u32 +
                        score.mods.contains(OsuMods::NoFail) as u32;

                    let max_value = match score_amount {
                        0 => MANIA_SCORES[0].1,
                        1 => MANIA_SCORES_HALF[0].1,
                        2 => MANIA_SCORES_QUARTER[0].1,
                        3 => MANIA_SCORES_EIGHTH[0].1,
                        _ => MANIA_SCORES[0].1,
                    };

                    if value == "1m" {
                        if max_value < 1_000_000 {
                            msg.channel_id.say(&ctx.http, format!("Cannot set a score of 1m with these mods: {}", score.mods.short_list())).await?;
                            return Ok(());
                        }

                        score.score = 1_000_000;
                    }
                    else {
                        let mut num_value: u32 = value[..value.len() - 1].parse().unwrap(); // SAFETY: Regex should ensure this is always a valid u32.
                        num_value *= 1000;

                        if max_value < num_value {
                            msg.channel_id.say(&ctx.http, format!("Cannot set a score of {} with these mods: {}", value, score.mods.short_list())).await?;
                            return Ok(());
                        }

                        score.score = num_value;
                    }
                }
                None => {
                    if let OsuMode::Mania = mode {
                        if is_default {
                            let score_amount = score.mods.contains(OsuMods::HalfTime) as u32 +
                                score.mods.contains(OsuMods::Easy) as u32 +
                                score.mods.contains(OsuMods::NoFail) as u32;

                            let max_value = match score_amount {
                                0 => MANIA_SCORES[0].1,
                                1 => MANIA_SCORES_HALF[0].1,
                                2 => MANIA_SCORES_QUARTER[0].1,
                                3 => MANIA_SCORES_EIGHTH[0].1,
                                _ => MANIA_SCORES[0].1,
                            };

                            score.score = max_value;
                        }
                    }
                },
            }

            score.calculate_pp();

            single_fc(ctx, msg, profile, old_score, score, params).await?;
        }
        None => {
            if is_score_recent && is_fc {
                msg.channel_id.say(&ctx.http, "Score is already an FC.").await?;
                    return Ok(());
            }

            default_fc(ctx, msg, profile, old_score, score).await?;
        },
    };

    Ok(())
}

async fn default_fc(ctx: &Context, msg: &Message, profile: OsuProfile, old_score: Option<OsuScore>, score: OsuScore) -> CommandResult {
    let emoji = score.mode.emoji(ctx.http.clone()).await;

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
        e
        .thumbnail(format!("{}{}{}", BEATMAPSET_THUMBNAIL_START, score.beatmap.beatmapset_id, BEATMAPSET_THUMBNAIL_END))
        .colour(Colour::from_rgb(228, 132, 250))
        .title(format!("{} [{}] ({}) {} - {} [{}]", emoji, score.mode.name(), score.beatmap.creator.clone(), score.beatmap.artist.clone(),
            score.beatmap.title.clone(), score.beatmap.diff_name.clone()))

        .description(format!("{}```CS:{:.1}    AR:{:.1}    OD:{:.1}    HP:{:.1}\n{:.2}*    BPM:{:.0}    {}```",
            if score.beatmap.convert { "[Convert]\n" } else { "" },
            score.beatmap.cs, score.beatmap.ar, score.beatmap.od, score.beatmap.hp, 
            score.beatmap.star_rating, score.beatmap.bpm, score.mods.short_list()))

        .author(|a| a
            .name(format!("{}", profile.username))
            .url(format!("{}/{}/{}", PROFILE_URL, score.user_id, score.mode.url()))
            .icon_url(format!("{}/{}", PROFILE_PIC_URL, score.user_id))
        );

        match score.mode {
            OsuMode::Standard => {
                let format: fn(&OsuScore) -> String = |s| {
                    let max_combo_text = if s.fc { " [FC]".to_owned() } else {
                        if s.beatmap.max_combo == 0 { "".to_owned() } else { format!("/{}", s.beatmap.max_combo) } 
                    };

                    format!("```{:2.2}%    {}pp    Combo:{}{}\n300: {}    100: {}    50:{}    Miss: {}```",
                    s.accuracy * 100.0, s.pp, s.max_combo, max_combo_text, s.count_300,
                    s.count_100, s.count_50, s.count_miss)
                };

                if let Some(o) = old_score { e.field("Current Play", format(&o), false); }

                let ss = {
                    let mut clone = score.clone();

                    clone.count_300 += clone.count_miss;
                    clone.count_300 += clone.count_100;
                    clone.count_300 += clone.count_50;
                    clone.count_100 = 0;
                    clone.count_100p = 0;
                    clone.count_50 = 0;
                    clone.count_miss = 0;

                    clone.max_combo = clone.beatmap.max_combo;
                    clone.fc = true;

                    clone.calculate_accuracy();
                    clone.calculate_pp();

                    clone
                };

                e.field("100% SS", format(&ss), false);

                let acc99 = {
                    let mut clone = score.clone();

                    clone.force_fc_accuracy(0.99);
                    clone.calculate_pp();

                    clone
                };

                let acc98 = {
                    let mut clone = score.clone();

                    clone.force_fc_accuracy(0.98);
                    clone.calculate_pp();

                    clone
                };

                let acc95 = {
                    let mut clone = score.clone();

                    clone.force_fc_accuracy(0.95);
                    clone.calculate_pp();

                    clone
                };

                if score.accuracy < 0.99 { e.field("99% Accuracy", format(&acc99), false); }
                if score.accuracy < 0.98 { e.field("98% Accuracy", format(&acc98), false); }
                if score.accuracy < 0.95 { e.field("95% Accuracy", format(&acc95), false); }
            }
            OsuMode::Mania => {
                let format: fn(&OsuScore) -> String = |s| {
                    format!("```Score: {}    {}pp```",
                        s.score.to_formatted_string(&Locale::en_GB), s.pp)
                };

                if let Some(o) = old_score { e.field("Current Play", format(&o), false); }

                let score_amount = score.mods.contains(OsuMods::HalfTime) as u32 +
                    score.mods.contains(OsuMods::Easy) as u32 +
                    score.mods.contains(OsuMods::NoFail) as u32;

                let scores = match score_amount {
                    0 => MANIA_SCORES,
                    1 => MANIA_SCORES_HALF,
                    2 => MANIA_SCORES_QUARTER,
                    3 => MANIA_SCORES_EIGHTH,
                    _ => MANIA_SCORES,
                };

                for s in scores.iter() {
                    if score.score >= s.1 {
                        continue;
                    }

                    let display = {
                        let mut clone = score.clone();

                        clone.score = s.1;

                        clone.calculate_pp();

                        clone
                    };

                    e.field(format!("Score -> {}", s.0), format(&display), false);
                }           
            }
            OsuMode::Taiko => {
                let format: fn(&OsuScore) -> String = |s| {
                    let max_combo_text = if s.fc { " [FC]".to_owned() } else {
                        if s.beatmap.max_combo == 0 { "".to_owned() } else { format!("/{}", s.beatmap.max_combo) } 
                    };

                    format!("```{:2.2}%    {}pp    Combo:{}{}\nGreat: {}    Good: {}     Miss: {}```",
                    s.accuracy * 100.0, s.pp, s.max_combo, max_combo_text,
                    s.count_300, s.count_100, s.count_miss)
                };

                if let Some(o) = old_score { e.field("Current Play", format(&o), false); }

                let ss = {
                    let mut clone = score.clone();

                    clone.count_300 += clone.count_miss;
                    clone.count_300 += clone.count_100;
                    clone.count_100 = 0;
                    clone.count_miss = 0;

                    clone.max_combo = clone.beatmap.max_combo;
                    clone.fc = true;

                    clone.calculate_accuracy();
                    clone.calculate_pp();

                    clone
                };

                e.field("100% SS", format(&ss), false);

                let acc99 = {
                    let mut clone = score.clone();

                    clone.force_fc_accuracy(0.99);
                    clone.calculate_pp();

                    clone
                };

                let acc98 = {
                    let mut clone = score.clone();

                    clone.force_fc_accuracy(0.98);
                    clone.calculate_pp();

                    clone
                };

                let acc95 = {
                    let mut clone = score.clone();

                    clone.force_fc_accuracy(0.95);
                    clone.calculate_pp();

                    clone
                };

                if score.accuracy < 0.99 { e.field("99% Accuracy", format(&acc99), false); }
                if score.accuracy < 0.98 { e.field("98% Accuracy", format(&acc98), false); }
                if score.accuracy < 0.95 { e.field("95% Accuracy", format(&acc95), false); }
            }

            OsuMode::CatchTheBeat => { 
                e.field("Catch", "FC not yet implemented.", false);
            }
        }

        e.url(format!("{}/{}#{}/{}", BEATMAP_URL, score.beatmap.beatmapset_id, score.mode.url(), score.beatmap_id))
    })).await?;

    Ok(())
}

async fn single_fc(ctx: &Context, msg: &Message, profile: OsuProfile, old_score: Option<OsuScore>, score: OsuScore, params: String) -> CommandResult {
    let emoji = score.mode.emoji(ctx.http.clone()).await;

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
        let map_info: fn(&OsuScore) -> String = |s| {
            format!("{}```CS:{:.1}    AR:{:.1}    OD:{:.1}    HP:{:.1}\n{:.2}*    BPM:{:.0}    {}```",
                if s.beatmap.convert { "[Convert]\n" } else { "" },
                s.beatmap.cs, s.beatmap.ar, s.beatmap.od, s.beatmap.hp, 
                s.beatmap.star_rating, s.beatmap.bpm, s.mods.short_list())
        };

        e
        .thumbnail(format!("{}{}{}", BEATMAPSET_THUMBNAIL_START, score.beatmap.beatmapset_id, BEATMAPSET_THUMBNAIL_END))
        .colour(Colour::from_rgb(228, 132, 250))
        .title(format!("{} [{}] ({}) {} - {} [{}]", emoji, score.mode.name(), score.beatmap.creator.clone(), score.beatmap.artist.clone(),
            score.beatmap.title.clone(), score.beatmap.diff_name.clone()))

        .description(format!("Custom Settings: {}", params))

        .author(|a| a
            .name(format!("{}", profile.username))
            .url(format!("{}/{}/{}", PROFILE_URL, score.user_id, score.mode.url()))
            .icon_url(format!("{}/{}", PROFILE_PIC_URL, score.user_id))
        );

        match score.mode {
            OsuMode::Standard => {
                let format: fn(&OsuScore) -> String = |s| {
                    let max_combo_text = if s.fc { " [FC]".to_owned() } else {
                        if s.beatmap.max_combo == 0 { "".to_owned() } else { format!("/{}", s.beatmap.max_combo) } 
                    };

                    format!("```{:2.2}%    {}pp    Combo:{}{}\n300: {}    100: {}    50:{}    Miss: {}```",
                    s.accuracy * 100.0, s.pp, s.max_combo, max_combo_text, s.count_300,
                    s.count_100, s.count_50, s.count_miss)
                };

                if let Some(o) = old_score { e.field("Current Play", format!("{} {}", map_info(&o), format(&o)), false); }
                e.field("Custom FC", format!("{} {}", map_info(&score), format(&score)), false);
            }
            OsuMode::Mania => {
                let format: fn(&OsuScore) -> String = |s| {
                    format!("```Score: {}    {}pp```",
                        s.score.to_formatted_string(&Locale::en_GB), s.pp)
                };

                if let Some(o) = old_score { e.field("Current Play", format!("{} {}", map_info(&o), format(&o)), false); }
                e.field("Custom FC", format!("{} {}", map_info(&score), format(&score)), false);
            }
            OsuMode::Taiko => {
                let format: fn(&OsuScore) -> String = |s| {
                    let max_combo_text = if s.fc { " [FC]".to_owned() } else {
                        if s.beatmap.max_combo == 0 { "".to_owned() } else { format!("/{}", s.beatmap.max_combo) } 
                    };

                    format!("```{:2.2}%    {}pp    Combo:{}{}\nGreat: {}    Good: {}     Miss: {}```",
                    s.accuracy * 100.0, s.pp, s.max_combo, max_combo_text,
                    s.count_300, s.count_100, s.count_miss)
                };

                if let Some(o) = old_score { e.field("Current Play", format!("{} {}", map_info(&o), format(&o)), false); }
                e.field("Custom FC", format!("{} {}", map_info(&score), format(&score)), false);
            }

            OsuMode::CatchTheBeat => {
                e.field("Catch", "FC not yet implemented.", false);
            }
        }

        e.url(format!("{}/{}#{}/{}", BEATMAP_URL, score.beatmap.beatmapset_id, score.mode.url(), score.beatmap_id))
    })).await?;

    Ok(())
}