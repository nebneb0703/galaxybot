use serenity::{

    framework::standard::{
        Args,
        CommandResult,
        macros::command
    }
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use crate::db::DbUser;
use crate::osu::*;

use diesel::SaveChangesDsl;

#[command("mode")]
#[description("Sets default mode for user.")]
#[usage("o!mode [mode]")]
pub async fn set_mode(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let mode = match args.current() {
        Some(a) => match OsuMode::parse(a) {
            Some(m) => m,
            None => OsuMode::Standard
        },
        None => OsuMode::Standard
    };

    let mut user = match database.get_user(*msg.author.id.as_u64()).await? {
        Some(u) => u,
        None => { 
            msg.channel_id.say(&ctx.http, "You need to first sync discord with osu!.").await?;
            return Ok(()); 
        },
    };

    user.default_mode = mode;

    let _: DbUser = user.save_changes(&*database.conn.lock().await)?;

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e        
        .title(format!("Updated default mode to {}", mode.name()))
        .colour(Colour::from_rgb(228, 132, 250))

        .author(|a| a
            .name(user.osu_username)
            .url(format!("{}/{}/{}", PROFILE_URL, user.osu_id.0, mode.url()))
            .icon_url(format!("{}/{}", PROFILE_PIC_URL, user.osu_id.0))
        )

        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    
    )).await?;


    Ok(())
}